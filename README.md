# README #

This repository is highly outdated and just a playground of my youth.

### What is this repository for? ###

This program is about approximations of images using triangles.

### How do I get set up? ###

You need CMake in order to create a Makefile or any project files.
Just run cmake . and make then to get started.
You can get help on how to use this program by running it with the
-h or --help parameters.

### Who do I talk to? ###

* Repo owner or admin