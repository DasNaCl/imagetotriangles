/*
    Copyright (C) 2014 Matthis Kruse

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef APPLICATION_HPP_INCLUDED
#define APPLICATION_HPP_INCLUDED

#include "Dimensions.hpp"
#include <string>
#include <vector>

namespace sf
{
    template<typename T>
    class Vector2;
    typedef Vector2<unsigned int> Vector2u;
}

class DNA;

class Application
{
public:
    Application() = default;

    int main(std::vector<std::string>& arguments);

private:
    bool readArguments(const std::vector<std::string>& args, std::vector<DIMENSIONS>& toRemove);
    void loadRDNA(DNA& rdna, const sf::Vector2u& imgSize);
    void printHelp();
    void readUint(const std::string& str, unsigned int& unint);

private:
    unsigned int _triangleNum {200U};
    unsigned int _interval {25U};
};

#endif // APPLICATION_HPP_INCLUDED
