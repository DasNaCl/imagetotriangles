/*
    Copyright (C) 2014 Matthis Kruse

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef TRIANGLE_HPP_INCLUDED
#define TRIANGLE_HPP_INCLUDED

#include <SFML/Graphics/Shape.hpp>
#include <array>
#include <istream>
#include <ostream>

class Triangle : public sf::Shape
{
public:
    Triangle();

    unsigned int getPointCount() const override;
    sf::Vector2f getPoint(unsigned int index) const override;

    void changeR(sf::Uint8 r);
    void changeG(sf::Uint8 g);
    void changeB(sf::Uint8 b);
    void changeA(sf::Uint8 a);
    void changeTr0(sf::Vector2i p);
    void changeTr1(sf::Vector2i p);
    void changeTr2(sf::Vector2i p);

    int getMostLeft();
    int getMostRight();
    int getMostUp();
    int getMostDown();

    void setTring(const std::array<sf::Vector2i, 3U>& pos);

    friend std::ostream& operator<<(std::ostream& os, const Triangle& triangle);

private:
    std::array<sf::Vector2i, 3U> _pos {};
};

#endif // TRIANGLE_HPP_INCLUDED
