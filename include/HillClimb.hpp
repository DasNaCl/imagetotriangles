/*
    Copyright (C) 2014 Matthis Kruse

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef HILLCLIMB_HPP_INCLUDED
#define HILLCLIMB_HPP_INCLUDED

#include "DNA.hpp"
#include "Dimensions.hpp"
#include "Screenshotter.hpp"
#include <SFML/Graphics/RenderTexture.hpp>

namespace sf
{
    class Image;
}

class HillClimb
{
public:
    HillClimb(const sf::Vector2u& size, const DNA& randomDna, const sf::Image& to);

    const DNA& getDNA();
    void step(const sf::Image& to, unsigned int interval, unsigned int frameNum);

    void rmDimension(DIMENSIONS d);

private:
    void modifyTr(Triangle& tr, DIMENSIONS d, int k);

private:
    sf::RenderTexture _rtex;
    DNA _dna;
    std::vector<DIMENSIONS> _dims;
    ScreenShotter _screenShotter;
};

#endif // HILLCLIMB_HPP_INCLUDED
