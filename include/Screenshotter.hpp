/*
    Copyright (C) 2014 Matthis Kruse

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef SCREENSHOTTER_HPP_INCLUDED
#define SCREENSHOTTER_HPP_INCLUDED

#include <SFML/Graphics/Image.hpp>
#include <boost/filesystem.hpp>
#include <atomic>
#include <thread>
#include <map>

class ScreenShotter
{
public:
    ScreenShotter();
    ~ScreenShotter();

    void add(const sf::Image& img);
private:
    void update();
public:
    static const unsigned int _maxThreadCount;
private:
    std::vector<unsigned int> _saved {};
    std::map<unsigned int, sf::Image> _toSave {};
    std::vector<std::thread> _threads {};
    std::atomic<unsigned int> _counter {0U};
    boost::filesystem::path _dir;
};

#endif // SCREENSHOTTER_HPP_INCLUDED
