/*
    Copyright (C) 2014 Matthis Kruse

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "../include/Application.hpp"
#include "../include/HillClimb.hpp"
#include <boost/scoped_ptr.hpp>
#include <SFML/Graphics/RenderWindow.hpp>
#include <SFML/Graphics/Font.hpp>
#include <SFML/Graphics/Text.hpp>
#include <SFML/Window/Event.hpp>
#include <iostream>
#include <fstream>
#include <sstream>
#include <random>
#include <tuple>

int Application::main(std::vector<std::string>& arguments)
{
    if(arguments.front() == "-h" || arguments.front() == "--help")
    {
        printHelp();
        return 0;
    }

    sf::Image img;
    if(!img.loadFromFile(arguments.front()))
    {
        throw std::invalid_argument("Couldn\'t find " + arguments.front());
    }
    arguments.erase(arguments.begin());
    DNA rdna;
    std::vector<DIMENSIONS> toRemove;
    if(!readArguments(arguments, toRemove))
        return 0;
    loadRDNA(rdna, img.getSize());

    sf::ContextSettings settings;
    settings.antialiasingLevel = 8;
    unsigned int appSizeX = img.getSize().x;
    if(appSizeX < 200)
        appSizeX = 200;
    sf::RenderWindow renderApp(sf::VideoMode(appSizeX, img.getSize().y + 20U), "ImageToTriangles", sf::Style::Default, settings);

    HillClimb hc(img.getSize(), rdna, img);
    if(!toRemove.empty())
    {
        for(auto& a : toRemove)
        {
            hc.rmDimension(a);
        }
    }

    sf::Font font;
    bool fontLoaded = font.loadFromFile("res/Raleway Thin.ttf");
    boost::scoped_ptr<sf::Text> statistic0(nullptr), statistic1(nullptr);
    if(fontLoaded)
    {
        statistic0.reset(new sf::Text("", font, 18U));
        statistic1.reset(new sf::Text("", font, 18U));

        statistic0->setPosition(renderApp.getSize().x * 0.05, renderApp.getSize().y - 20U);
        statistic1->setPosition(renderApp.getSize().x * 0.54, renderApp.getSize().y - 20U);
    }

    unsigned int frameCount = 0U;
    while(renderApp.isOpen())
    {
        sf::Event event;
        while(renderApp.pollEvent(event))
        {
            if(event.type == sf::Event::Closed)
                renderApp.close();
        }

        hc.step(img, _interval, frameCount++);

        renderApp.clear();
        renderApp.draw(hc.getDNA());
        if(fontLoaded)
        {
            std::stringstream ss;
            ss << "Q: " << hc.getDNA()._quality;
            statistic0->setString(ss.str());
            ss.str("");

            ss << "F: " << frameCount;
            statistic1->setString(ss.str());

            renderApp.draw(*statistic0);
            renderApp.draw(*statistic1);
        }
        renderApp.display();
    }

    return 0;
}

bool Application::readArguments(const std::vector<std::string>& args, std::vector<DIMENSIONS>& toRemove)
{
    unsigned int lastCommand = 0U;
    for(auto& a : args)
    {
        if(lastCommand == 1U)
        {
            readUint(a, _interval);
            if(_interval == 0U)
                throw std::invalid_argument("Interval must be at least one.\nIf you don\'t want any screenshot, set this very high.");

            lastCommand = 0U;
            continue;
        }
        else if(lastCommand == 2U)
        {
            readUint(a, _triangleNum);
            if(_triangleNum == 0U)
                throw std::invalid_argument("Triangle number must be at leat one.");

            lastCommand = 0U;
            continue;
        }
        else if(lastCommand == 3U)
        {
            std::stringstream ss(a);
            unsigned int tmp = 0U;
            if(!(ss >> tmp))
                throw std::invalid_argument("Cannot convert \"" + a + "\" to an integer.");

            tmp += static_cast<unsigned int>(DIMENSIONS::RED);
            if(tmp > static_cast<unsigned int>(DIMENSIONS::ALPHA))
                throw std::invalid_argument("This is no known search-field.");

            toRemove.push_back(static_cast<DIMENSIONS>(tmp));

            lastCommand = 0U;
            continue;
        }

        if(a == "-i"
        || a == "--interval")
        {
            lastCommand = 1U;
        }
        else if(a == "-tn"
        || a == "--triangle-num")
        {
            lastCommand = 2U;
        }
        else if(a == "-rs"
             || a == "--remove-search")
        {
            lastCommand = 3U;
        }
        else
        {
            printHelp();

            return false;
        }
    }

    return true;
}

void Application::loadRDNA(DNA& rdna, const sf::Vector2u& imgSize)
{
    std::random_device rd;
    std::mt19937 mt(rd());
    std::uniform_int_distribution<unsigned int> posDistX(0U, imgSize.x);
    std::uniform_int_distribution<unsigned int> posDistY(0U, imgSize.y);
    std::uniform_int_distribution<sf::Uint8> colDist(0U, 255U);

    for(std::size_t i = 0U; i < _triangleNum; ++i)
    {
        Triangle tr;
        tr.changeR(colDist(mt));
        tr.changeG(colDist(mt));
        tr.changeB(colDist(mt));
        tr.changeA(colDist(mt));

        std::array<sf::Vector2i, 3U> thePos;
        for(auto& p : thePos)
        {
            p.x = posDistX(mt);
            p.y = posDistY(mt);
        }
        std::sort(std::begin(thePos), std::end(thePos), [](sf::Vector2i p0, sf::Vector2i p1){return p0.x < p1.x;});
        tr.setTring(thePos);

        rdna._data.push_back(tr);
    }
}

void Application::printHelp()
{
    std::cout << "./ImageToTriangles [PathToImage] [Arguments]\n";
    std::cout << "Possible Arguments:\n";
    std::cout << "-h --help\t\tPrint this help.\n";
    std::cout << "-i --interval\t\tChange the Screenshot interval. Must be at least 1.\n";
    std::cout << "-tn --triangle-num\tChange the triangle num. Must be at least 1.\n";
    std::cout << "-rs --remove-search\tRemove a specific search field. e.g. do not change alpha values.\n\t\t\tOnly provide ONE (argument can be used multiple times)\n\t\t\tof these modifiers: (0: red|1: blue|2: green|3: alpha)";
    std::cout << std::endl;
}

void Application::readUint(const std::string& str, unsigned int& unint)
{
    std::stringstream ss(str);
    if(!(ss >> unint))
        throw std::invalid_argument("Cannot convert \"" + str + "\" to an integer.");
}
