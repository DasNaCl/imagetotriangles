/*
    Copyright (C) 2014 Matthis Kruse

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "../include/Climber.hpp"
#include <SFML/Graphics/Image.hpp>
#include <stdexcept>
#include <vector>

double Climber::cmp(const sf::Image& current, const sf::Image& target)
{
    if(current.getSize().x != target.getSize().x
    || current.getSize().y != target.getSize().y)
    {
        throw std::invalid_argument("Images need to be equal size!");
    }

    if(current.getSize().x <= 3U
    || current.getSize().y <= 3U)
    {
        throw std::invalid_argument("Size needs to be greater than 3.");
    }

    const unsigned int size = current.getSize().x * current.getSize().y * 4U;
    const sf::Uint8* curPixels = current.getPixelsPtr();
    const sf::Uint8* tarPixels =  target.getPixelsPtr();

    double sum = 0.;
    for(unsigned int i = 0U; i < size; ++i)
    {
        double from = static_cast<double>(curPixels[i]) + 1.;
        double to = static_cast<double>(tarPixels[i]) + 1.;

        sum += std::fabs(to - from) / 256.0;
    }

    return 100.0 - (sum / size) * 100.0;
}
