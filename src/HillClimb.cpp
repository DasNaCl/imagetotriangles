/*
    Copyright (C) 2014 Matthis Kruse

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    butWITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "../include/HillClimb.hpp"
#include "../include/Climber.hpp"
#include <stdexcept>
#include <sstream>

HillClimb::HillClimb(const sf::Vector2u& size, const DNA& randomDna, const sf::Image& to)
: _rtex(), _dna(randomDna),
  _dims({DIMENSIONS::X0, DIMENSIONS::Y0, DIMENSIONS::X1, DIMENSIONS::Y1, DIMENSIONS::X2, DIMENSIONS::Y2,
         DIMENSIONS::RED, DIMENSIONS::GREEN, DIMENSIONS::BLUE, DIMENSIONS::ALPHA}), _screenShotter()
{
    if(!_rtex.create(size.x, size.y))
    {
        std::stringstream ss;
        ss << "Not able to create a texture of size " << size.x << "x" << size.y << ".";
        throw std::invalid_argument(ss.str());
    }

    _rtex.clear();
    _rtex.draw(_dna);
    _rtex.display();

    _dna._quality = Climber::cmp(_rtex.getTexture().copyToImage(), to);
}

const DNA& HillClimb::getDNA()
{
    return _dna;
}

void HillClimb::step(const sf::Image& to, unsigned int interval, unsigned int frameNum)
{
    for(std::size_t j = 0U; j < _dna._data.size(); ++j)
    {
        for(int k = -1; k < 2; ++k)
        {
            for(auto a : _dims)
            {
                if((a == DIMENSIONS::X0 || a == DIMENSIONS::X1 || a == DIMENSIONS::X2)
                && (_dna._data[j].getMostLeft() + k < 0
                 || _dna._data[j].getMostRight() + k > static_cast<int>(to.getSize().x)))
                {
                    continue;
                }
                else if((a == DIMENSIONS::Y0 || a == DIMENSIONS::Y1 || a == DIMENSIONS::Y2)
                && (_dna._data[j].getMostUp() + k < 0
                 || _dna._data[j].getMostDown() + k > static_cast<int>(to.getSize().y)))
                {
                    continue;
                }

                DNA otherdna = _dna;

                modifyTr(otherdna._data[j], a, k);

                _rtex.clear();
                _rtex.draw(otherdna);
                _rtex.display();

                otherdna._quality = Climber::cmp(_rtex.getTexture().copyToImage(), to);

                if(otherdna._quality > _dna._quality)
                    _dna = otherdna;
            }
        }
    }

    if(frameNum % interval == 0U)
    {
        _rtex.clear();
        _rtex.draw(_dna);
        _rtex.display();

        _screenShotter.add(_rtex.getTexture().copyToImage());
    }
}

void HillClimb::rmDimension(DIMENSIONS d)
{
    auto it = std::find(std::begin(_dims), std::end(_dims), d);

    if(it != std::end(_dims))
        _dims.erase(it);
}

void HillClimb::modifyTr(Triangle& tr, DIMENSIONS d, int k)
{
    switch(d)
    {
    case DIMENSIONS::X0:    tr.changeTr0(sf::Vector2i(tr.getPoint(0U).x + k, tr.getPoint(0U).y)); break;
    case DIMENSIONS::Y0:    tr.changeTr0(sf::Vector2i(tr.getPoint(0U).x, tr.getPoint(0U).y + k)); break;
    case DIMENSIONS::X1:    tr.changeTr1(sf::Vector2i(tr.getPoint(1U).x + k, tr.getPoint(1U).y)); break;
    case DIMENSIONS::Y1:    tr.changeTr1(sf::Vector2i(tr.getPoint(1U).x, tr.getPoint(1U).y + k)); break;
    case DIMENSIONS::X2:    tr.changeTr2(sf::Vector2i(tr.getPoint(2U).x + k, tr.getPoint(2U).y)); break;
    case DIMENSIONS::Y2:    tr.changeTr2(sf::Vector2i(tr.getPoint(2U).x, tr.getPoint(2U).y + k)); break;
    case DIMENSIONS::RED:   tr.changeR(tr.getFillColor().r + k); break;
    case DIMENSIONS::GREEN: tr.changeG(tr.getFillColor().g + k); break;
    case DIMENSIONS::BLUE:  tr.changeB(tr.getFillColor().b + k); break;
    case DIMENSIONS::ALPHA: tr.changeA(tr.getFillColor().a + k); break;

    case DIMENSIONS::END:
    default: throw std::logic_error("What are you trying to do?");
    }
}
