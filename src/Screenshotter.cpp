/*
    Copyright (C) 2014 Matthis Kruse

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "../include/Screenshotter.hpp"
#include <sstream>

const unsigned int ScreenShotter::_maxThreadCount = std::thread::hardware_concurrency();

ScreenShotter::ScreenShotter() : _saved(), _toSave(), _threads(), _counter(0U), _dir(std::string("images") + boost::filesystem::unique_path().c_str())
{
    while(boost::filesystem::exists(_dir))
    {
        _dir = std::string("images") + boost::filesystem::unique_path().c_str();
    }
    boost::filesystem::create_directory(_dir);

    for(unsigned int i = 0U; i < ScreenShotter::_maxThreadCount; ++i)
        _threads.push_back(std::thread());
}

ScreenShotter::~ScreenShotter()
{
    for(auto& t : _threads)
        if(t.joinable())
            t.join();
}

void ScreenShotter::add(const sf::Image& img)
{
    _toSave[_counter++] = img;

    update();
}

void ScreenShotter::update()
{
    for(auto& t : _threads)
    {
        if(!t.joinable() && !_toSave.empty())
        {
            bool foundSome = true;
            unsigned int val = 0U;
            unsigned int C = _toSave.begin()->first;
            do
            {
                   //hehehe
                val = C++;
                if(val >= _counter)
                {
                    foundSome = false;
                    break;
                }
            }while(std::find(std::begin(_saved), std::end(_saved), val) != std::end(_saved));

            if(foundSome)
            {
                _saved.push_back(C - 1U);
                t = std::thread([this,C]()
                {
                    std::stringstream ss;
                    ss << _dir.c_str() << "/img" << (C - 1U) << ".png";

                    _toSave[C - 1U].saveToFile(ss.str());

                    _saved.erase(std::find(std::begin(_saved), std::end(_saved), C - 1U));
                    _toSave.erase(_toSave.find(C - 1U));
                });
                t.join();
            }
            else
            {
                break;
            }
        }
    }
}
