/*
    Copyright (C) 2014 Matthis Kruse

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "../include/Triangle.hpp"
#include <sstream>

Triangle::Triangle() : Shape()
{
    setFillColor(sf::Color::Black);
}

unsigned int Triangle::getPointCount() const
{
    return 3U;
}

sf::Vector2f Triangle::getPoint(unsigned int index) const
{
    return static_cast<sf::Vector2f>(_pos[index % 3U]);
}

void Triangle::changeTr0(sf::Vector2i p)
{
    _pos[0] = p;

    update();
}

void Triangle::changeTr1(sf::Vector2i p)
{
    _pos[1] = p;

    update();
}

void Triangle::changeTr2(sf::Vector2i p)
{
    _pos[2] = p;

    update();
}

int Triangle::getMostLeft()
{
    return std::min_element(std::begin(_pos), std::end(_pos),
    [](sf::Vector2i lhs, sf::Vector2i rhs)->bool
    {
        return lhs.x < rhs.x;
    })->x;
}

int Triangle::getMostRight()
{
    return std::max_element(std::begin(_pos), std::end(_pos),
    [](sf::Vector2i lhs, sf::Vector2i rhs)->bool
    {
        return lhs.x < rhs.x;
    })->x;
}

int Triangle::getMostUp()
{
    return std::min_element(std::begin(_pos), std::end(_pos),
    [](sf::Vector2i lhs, sf::Vector2i rhs)->bool
    {
        return lhs.y < rhs.y;
    })->y;
}

int Triangle::getMostDown()
{
    return std::max_element(std::begin(_pos), std::end(_pos),
    [](sf::Vector2i lhs, sf::Vector2i rhs)->bool
    {
        return lhs.y < rhs.y;
    })->y;
}

void Triangle::changeR(sf::Uint8 r)
{
    setFillColor(sf::Color(r,
                           getFillColor().g,
                           getFillColor().b,
                           getFillColor().a)
                );
}

void Triangle::changeG(sf::Uint8 g)
{
    setFillColor(sf::Color(getFillColor().r,
                           g,
                           getFillColor().b,
                           getFillColor().a)
                );
}

void Triangle::changeB(sf::Uint8 b)
{
    setFillColor(sf::Color(getFillColor().b,
                           getFillColor().g,
                           b,
                           getFillColor().a)
                );
}

void Triangle::changeA(sf::Uint8 a)
{
    setFillColor(sf::Color(getFillColor().r,
                           getFillColor().g,
                           getFillColor().b,
                           a)
                );
}

void Triangle::setTring(const std::array<sf::Vector2i, 3U>& pos)
{
    _pos = pos;

    update();
}

std::ostream& operator<<(std::ostream& os, const Triangle& triangle)
{
    for(unsigned int i = 0U; i < 3U; ++i)
    {
        os << triangle._pos[i].x << " " << triangle._pos[i].y << "!";
    }

    return os;
}
